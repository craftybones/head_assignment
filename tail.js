const shelljs=require('shelljs');

let args=process.argv.slice(2).join(" ");
let output=shelljs.exec(`tail ${args}`,{silent:true});
if(output.stderr) {
  console.log(output.stderr);
} 
console.log(output.stdout);
